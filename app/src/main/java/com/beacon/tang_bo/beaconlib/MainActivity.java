package com.beacon.tang_bo.beaconlib;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {   //변수 초기화 
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("debug_lifeCycle", "onCreate");
    }

    @Override
    protected void onStart(){   //
        super.onStart();
        Log.d("debug_lifeCycle","onStart");
    }
    @Override
    protected void onResume(){  //
        super.onResume();
        Log.d("debug_lifeCycle", "onResume");
    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.d("debug_lifeCycle", "onPause");
    }
    @Override
    protected void onStop(){
        super.onStop();
        Log.d("debug_lifeCycle", "onStop");
    }
    @Override
    protected void onRestart(){
        super.onRestart();
        Log.d("debug_lifeCycle", "onRestart");
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d("debug_lifeCycle", "onDestroy");
    }

}
